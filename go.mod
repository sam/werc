module git.froth.zone/sam/go2werc

go 1.19

require (
	github.com/russross/blackfriday/v2 v2.1.0
	golang.org/x/tools v0.19.0
)
